# -*- coding:utf-8 -*-
from email import message
from glob import glob
import threading
import re
from unittest import expectedFailure
import requests
import socket
import json
from time import sleep
from typing import List
import random
import sys
from math import *
from func_timeout import func_set_timeout
import numpy as np
# 预存数据

allSociety = "天文社\n模拟联合国\n忘忧棋社\n漫萌社\n辩协\n南风民乐社\n魔方社\nPM模拟节目制作社\nKey声乐社\n博雅汉风社\nFriday 广播社\nStage 话剧社\nsunnyworld\nNewland 杂志社\npenbeat\n 无线电测向社\nEncore街舞社\nIPA\niMakeit机器人社\n二泉文学社\npfm演绎社\n密码社\n乾元武道社\nTalkshow\n龙鹤相声社\n志愿者协会\nNewAge管弦乐社\n心理协会\nBEE生物社\n园艺社\n羽毛球社\n伽绘绘画社\n书法社"
societyList = ["天文社","模拟联合国","忘忧棋社","漫萌社","辩协","南风民乐社","魔方社","PM模拟节目制作社","Key声乐社","博雅汉风社","Friday 广播社","Stage 话剧社","sunnyworld","Newland 杂志社","penbeat"," 无线电测向社","Encore街舞社","IPA","iMakeit机器人社","二泉文学社","pfm演绎社","密码社","乾元武道社","Talkshow","龙鹤相声社","志愿者协会","NewAge管弦乐社","心理协会","BEE生物社","园艺社","羽毛球社","伽绘绘画社","书法社"]
societyStars = {"天文社": 0 ,"模拟联合国": 0 ,"忘忧棋社": 0 ,"漫萌社": 0 ,"辩协": 0 ,"南风民乐社": 0 ,"魔方社": 0 ,"PM模拟节目制作社": 0 ,"Key声乐社": 0 ,"博雅汉风社": 0 ,"Friday 广播社": 0 ,"Stage 话剧社": 0 ,"sunnyworld": 0 ,"Newland 杂志社": 0 ,"penbeat": 0 ," 无线电测向社": 0 ,"Encore街舞社": 0 ,"IPA": 0 ,"iMakeit机器人社": 0 ,"二泉文学社": 0 ,"pfm演绎社": 0 ,"密码社": 0 ,"乾元武道社": 0 ,"Talkshow": 0 ,"龙鹤相声社": 0 ,"志愿者协会": 0 ,"NewAge管弦乐社": 0 ,"心理协会": 0 ,"BEE生物社": 0 ,"园艺社": 0 ,"羽毛球社": 0 ,"伽绘绘画社": 0 ,"书法社": 0 }

# 缓存读取

societyStars = np.load('societyStarts.npy', allow_pickle=True).item()
def updateSocietyStars():
    global societyStars
    np.save('societyStarts.npy', societyStars)

# 正则表达式
# 言论过滤
re_talk=".*[你|尼][妈|码].*|.*[傻|伞][逼|兵].*|.*共产党.*|.*妈妈的.*|.*二百五.*|.*中共.*"
# 昵称合规性检查
re3 = ".*[新|准][高|大][一|二|三|四].*|.*社.*|.*部长|一中学生地带|.*(高[一|二|三]).*|.*学生会.*|IMakeIt友情赞助段景昊的机器人|学生发展中心主席宗可为|足协主席 Friday播音 心协 sw iMakeit 彭康|.*国际部.*|.*iMakeit.*|IPA.*|鲍静老师|.*无锡一中.*"
reTime = r"\] (.*?)s"
reid = r"\[CQ:at,qq=(.*?)\]"
reCode = "code:(.*?);"
reMath = "算术:(.*?)="
reD = "d(.*?);"
reAt = r"\[CQ:at,qq=3435059650\] |@IMakeIt友情赞助段景昊的机器人 "
reStar = "为(.*?)打call"
reGradeTen = ".*高一.*"
#print输出重定向

class __Autonomy__(object):
    """
    自定义变量的write方法
    """
    def __init__(self):
        """
        init
        """
        self._buff = ""

    def write(self, out_stream):
        """
        :param out_stream:
        :return:
        """
        self._buff += out_stream


# list重复内容获取


def getIndex(lst=None, item=''):
    tmp = []
    tag = 0
    for i in lst:
        if i == item:
             tmp.append(tag)
        tag += 1
    return tmp


# 基础发送消息函数


def sendMsg(msg, qq_type="group", id=811435872):
    if qq_type == "private":
        data = {
            'user_id': id,
            'message': msg,
            'auto_escape': False
        }
    elif qq_type == "group":
        data = {
            'group_id': id,
            'message': msg,
            'auto_escape': False
        }
    else:
        return False
    cq_url = "http://127.0.0.1:5700/send_group_msg"
    requests.post(cq_url, data=data)


# 基础获取群人员列表


def getGroup(id=811435872):
    date = []
    response = requests.post(
        'http://127.0.0.1:5700/get_group_member_list?group_id='+str(id)).json()
    for i in response['data']:
        temp = ""
        if(i['card'] != ''):
            temp = i['card']  # +str(i['user_id'])
        else:
            temp = i['nickname']  # +str(i['user_id'])
        date.append(temp)
    return date


# 基础撤回消息


def delMsg(msgid):
    data = {
        'message_id': msgid
    }
    cq_url = "http://127.0.0.1:5700/delete_msg"
    requests.post(cq_url, data=data)


# 基础禁言功能


def ban(user_id,time):
    data = {
        'user_id': user_id,
        'group_id': 811435872,
        'duration':time
    }
    cq_url = "http://127.0.0.1:5700/set_group_ban"
    requests.post(cq_url, data=data)    


# 检查是否正确群昵称


def checkName():
    date = getGroup()
    temp = ""
    for i in date:
        if re.match(re3, i) == None:
            temp = temp+"@"+i+"\n"
        elif re.match(reGradeTen,i) != None:
            if bool(re.search('[a-z]|[A-Z]', i)):
                temp = temp+"@"+i+"\n"
    print(temp)
    sendMsg("以下同学名字不符合规定请尽快修改:\n提示：新生请改为：新高一xxx\n高二请改为：新高二（社团信息）xxx\n")
    sendMsg(temp)


# 全员禁言


def banall(enable):
    data = {
        "enable": enable,
        'group_id': 811435872
    }
    cq_url = "http://127.0.0.1:5700/set_group_whole_ban"
    requests.post(cq_url, data=data)    


# 踢人


def kick(user_id):
    data = {
        "user_id": user_id,
        'group_id': 811435872
    }
    cq_url = "http://127.0.0.1:5700/set_group_kick"
    requests.post(cq_url, data=data)  


# 获取社团点赞


def getSocietyStarts():
    temp=""
    for key in societyStars:
        temp = temp + key + ":" +str(societyStars[key]) + "\n"
    return temp

# math运行函数

@func_set_timeout(5)
def runMath(s):
    return eval(s)

# python运行函数

@func_set_timeout(5)
def runPython(s):
    exec(s)

# 消息接收部分


encoding = 'utf-8'
BUFSIZE = 1024
temp = ""

# a read thread, read data from remote

class Reader(threading.Thread):
    global temp

    def __init__(self, client):
        threading.Thread.__init__(self)
        self.client = client

    def run(self):
        global temp
        while True:
            data = self.client.recv(BUFSIZE)
            if(data):
                temp = bytes.decode(data, encoding)
            else:
                break

    def readline(self):
        rec = self.inputs.readline()
        if rec:
            string = bytes.decode(rec, encoding)
            if len(string) > 2:
                string = string[0:-2]
            else:
                string = ' '
        else:
            string = False
        return string


# a listen thread, listen remote connect
# when a remote machine request to connect, it will create a read thread to handle

class Listener(threading.Thread):
    def __init__(self, port):
        threading.Thread.__init__(self)
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(("0.0.0.0", port))
        self.sock.listen(1)

    def run(self):
        print("listener started")
        while True:
            client, cltadd = self.sock.accept()
            Reader(client).start()
            cltadd = cltadd


#消息监听


lst = Listener(25567)   # create a listen thread
lst.start()  # then start


# 消抖


msgidTemp = []
increaseTemp = []
msgTemp = []
useridTemp =[]

#循环监听


while True:

    #防止溢出

    if len(msgidTemp) > 50:
        msgidTemp = msgidTemp[25:]
    if len(msgidTemp) > 50:
        increaseTemp = increaseTemp[25:]
    if len(msgTemp) > 50:
        msgTemp = msgTemp[25:]

    #忽略所有异常保证程序稳定性

    try:
        if temp != "":
            temp = str(temp)
            temp = temp[24*7:]
            rev = json.loads(temp)
            
            #新生欢迎
            
            if rev["post_type"] == "notice":
                user_id = rev["user_id"]
                if (rev["notice_type"] == "group_increase") and (user_id not in increaseTemp):
                    sendMsg("[CQ:at,qq={}] 欢迎来到无锡一中，进群请改名字\n格式：年级+真实姓名".format(rev["user_id"]))
                #消抖

                increaseTemp.append(user_id)
            elif rev["post_type"] == "message":

                #信息处理

                msg = rev["message"]
                msgid = rev["message_id"]
                time = int(rev["time"])
                user_id = rev["user_id"]
                role = rev["sender"]["role"]
                if rev["sender"]["card"] == "":
                    name = rev["sender"]["nickname"]
                else:
                    name = rev["sender"]["card"]
                print("get message")
                if msgid not in msgidTemp:
                    print("收到一条新消息")
                    print(temp)

                    #消抖

                    msgidTemp.append(msgid)
                    msgTemp.append(msg)
                    useridTemp.append(user_id)

                    #判断恶意刷屏

                    if msgTemp.count(msg) > 4:
                        countTemp = msgTemp.count(msg)
                        for i in getIndex(msgTemp,msg):
                            if useridTemp[i] != user_id:
                                countTemp = countTemp - 1
                        if countTemp > 4:
                            sendMsg("[CQ:at,qq={}] 请不要恶意刷屏".format(user_id))
                            ban(user_id,60*5)

                    #聊天信息判断

                    if not(re.match(re_talk, msg) == None):
                        sendMsg("[CQ:at,qq={}] 这条消息不符合规范".format(user_id))
                        delMsg(msgid)

                    #判断群昵称规范性

                    if re.match(re3, name) == None:
                        sendMsg("[CQ:at,qq={}] 请按照要求修改群昵称".format(user_id))

                    #聊天功能

                    if ("[CQ:at,qq=3435059650]" in msg) or ("@IMakeIt友情赞助段景昊的机器人" in msg):
                        print("我需要回复这条消息")
                        msg = re.sub(reAt,"",msg)
                        #功能性功能

                        if "关机" in msg:
                            sendMsg("再见")
                        elif "查询群成员改名情况" in msg:
                            checkName()
                        elif "help" in msg:
                            sendMsg("命令列表：\n1、查询群成员改名情况\n2、关机\n3、欢迎鲍老板\n4、禁言:(用法：禁言@_ _s)\n5、全员禁言\n6、解除全员禁言\n7、踢人(用法:踢人@_)\n8、算术(用法:算术:_=)\n9、随机数(用法:随机数d_;)\n10、社团列表\n11、一中简介\n12、执行Python(用法:Python:(换行写代码))\n13、社团点赞列表\nPS:想要了解哪个社团只需要 @ 我+对应社团的名字！")
                        elif ("全员禁言" in msg)and(role != "member"):
                            banall("true")
                        elif ("解除全员禁言" in msg)and(role != "member"):
                            banall("false")
                        elif ("全员禁言" in msg)and(role == "member"):
                            sendMsg("[CQ:at,qq={}] 非管理员不可操作".format(user_id))
                        elif ("解除全员禁言" in msg)and(role == "member"):
                            sendMsg("[CQ:at,qq={}] 非管理员不可操作".format(user_id))
                        elif ("禁言" in msg)and(role !="member"):
                            banid=re.findall(reid,msg)[0]
                            banTime=re.findall(reTime,msg)[0]
                            if (banid!="")or(banTime!=""):
                                sendMsg("[CQ:at,qq={}] 已禁言[CQ:at,qq={}] {}秒".format(user_id,banid,banTime))
                                ban(banid,banTime)
                        elif ("禁言" in msg)and(role == "member"):
                            banid=re.findall(reid,msg)[0]
                            banTime=int(re.findall(reTime,msg)[0])
                            if int(banTime)>20:
                                sendMsg("[CQ:at,qq={}] 普通成员只能禁言20s以下哦".format(user_id))
                            elif int(banTime) == 0:
                                sendMsg("[CQ:at,qq={}] 普通成员不能解除禁言".format(user_id))
                            elif (banid!="")or(banTime!=""):
                                sendMsg("[CQ:at,qq={}] 已禁言[CQ:at,qq={}] {}秒".format(user_id,banid,banTime))
                                ban(banid,banTime)
                        elif ("踢人" in msg)and(role != "member"):
                            kickid=re.findall(reid,msg)[0]
                            sendMsg("[CQ:at,qq={}] 已删除[CQ:at,qq={}]".format(user_id,kickid))
                            kick(kickid)
                        elif ("踢人" in msg)and(role == "member"):
                            kickid=re.findall(reid,msg)[0]
                            sendMsg("[CQ:at,qq={}] 非管理员不可操作".format(user_id))
                            
                        elif "算术" in msg:
                            math = re.findall(reMath,msg)[0]
                            math = math.replace("π","pi")
                            math = math.replace("^","**")
                            try:
                                mathResult=runMath(math)
                                sendMsg("[CQ:at,qq={}] 我猜，答案是{}\n我是不是很聪明？快夸我！！".format(user_id,mathResult))
                            except:
                                sendMsg("[CQ:at,qq={}] 错误的表达式".format(user_id))
                        elif "随机数" in msg:
                            randomResult=re.findall(reD,msg)[0]
                            sendMsg("[CQ:at,qq={}] 随机数：{}".format(user_id,random.randint(1,int(randomResult))))
                        elif "社团列表" in msg:
                            sendMsg("[CQ:image,file=http://download.imakeit.tech/qq-robot-2023/社团表.png]")
                        elif "一中简介" in msg:
                            sendMsg("百年的梦想，百年的追求，新百年的征程已在一中人的脚下延伸……\n读书、明理、求进”诠释着无锡一中对教育本真意义的理解与行动；\n独立、协作、创造” 传递着一中人对学校社会价值的责任与担当。")
                        elif ("Python" in msg)or("python" in msg):
                            PythonCode = msg[8:]
                            try:
                                current = sys.stdout
                                a = __Autonomy__()
                                sys.stdout = a
                                runPython(PythonCode)
                                sendMsg("[CQ:at,qq={}] 你的Python代码运行成功，输出为：{}".format(user_id,a._buff))
                                sys.stdout = current
                            except:
                                sendMsg("[CQ:at,qq={}] 错误的表达式或超时".format(user_id))
                                sys.stdout = current
                        elif ("c++" in msg)or("C++" in msg):
                            PythonCode = msg[5:]
                            try:
                                sys
                            except:
                                sendMsg("[CQ:at,qq={}] 错误的表达式或超时".format(user_id))
                                sys.stdout = current
                        
                        #社团点赞
                        elif "社团点赞列表" in msg:
                            sendMsg(getSocietyStarts()+"PS:点赞方式:为___打call")   
                        elif "打call" in msg:
                            onStaredSociety = re.findall(reStar,msg)[0]
                            societyStars[onStaredSociety] = societyStars[onStaredSociety] +1
                            sendMsg("[CQ:at,qq={}] 感谢为{}打call".format(user_id,onStaredSociety))
                            updateSocietyStars()
                        #社团相关功能

                        elif ("iMakeit" in msg)or("机器人社" in msg)or("IMakeit" in msg)or("IMakeIt" in msg)or("imakeit" in msg):
                            sendMsg("我出生在iMakeit，快来iMakeit机器人社！")
                        elif "社联" in msg:
                            sendMsg("快来见鲍妈咪")
                        elif "有多少社团" in msg:
                            sendMsg("[CQ:image,file=http://download.imakeit.tech/qq-robot-2023/社团表.png]")
                        elif ("PFM" in msg)or("演绎社" in msg)or("pfm" in msg):
                            sendMsg("我们社比较nb\n快来！！！！")
                        elif ("生物社" in msg)or("BEE" in msg):
                            sendMsg("我社确实生竞人会比较多")
                        elif (("Sunny" in msg)or("sunny" in msg))and(("World" in msg)or("world" in msg))or("sw" == msg):
                            sendMsg("SunnyWorld是一中最成熟的社团之一，就像它的名字一样，阳光、青春、洋溢着正能量。每年的社团招新大会都能为SW带来炙热的新鲜血液，经验丰富的学长学姐和朝气蓬勃的新成员们共同为着SW和自己热爱的音乐探索。虽然各位社员性格各异，但彼此对音乐的热爱却把无数颗炽热的心连结在了一起。。SW为我们提供了一个用音乐去沟通灵魂的地方，如家，我们都是家人。比起职业的乐队，它更像是一片沃土，它是热爱音乐的我们的启蒙老师和探索的开始。一路走来，我们一直在尝试创新，以灵魂注入我们亲爱的，热爱的音乐，但SW的初衷永远不会暗淡，SW是快乐的，是个简单而又不简单的社团!")
                        elif ("Newland" in msg)or("newland" in msg):
                            sendMsg("Newland杂志社创立于2009年，至今已走过了十余个年头。自创社以来，Newland一直致力于在字里行间传递一中文化，承载红楼真情。作为历年来每次都被评为校十佳社团的老牌社团，Newland不断探索，在将时代的符号与纸媒相结合的同时，积极尝试官方QQ、微信公众号推送等多平台的展示")
                        elif "天文社" in msg:
                            sendMsg("机器人作者的主观评价：设备很全，有很贵的望远镜！")
                        elif "模拟联合国" in msg:
                            sendMsg("机器人作者的主观评价：好像很厉害")
                        elif ("棋社" in msg)or("忘忧" in msg):
                            sendMsg("机器人作者的主观评价：下棋的")
                        elif "漫萌社" in msg:
                            sendMsg("漫萌社简称DMS，是气氛轻松活跃的二次元亚文化社团。不论你是浓度极高成分复杂的大佬，还是仅仅对二次元文化感兴趣的萌新，在这里都能找到同好！！")
                        elif "辩协" in msg:
                            sendMsg("机器人作者的主观评价：很厉害，说不过他们")
                        elif "南风民乐社" in msg:
                            sendMsg("机器人作者的主观评价：很厉害")
                        elif "魔方社" in msg:
                            sendMsg("机器人作者的主观评价：不常见不过魔方我是真的不会")
                        elif "PM" in msg:
                            sendMsg("机器人作者的主观评价：不认识")
                        elif ("Key" in msg)or("key" in msg)or("声乐社" in msg):
                            sendMsg("key声乐社简介——\n本社建立于2015年，在一中校园里走过了7个春秋。从流行到美声，从清新到热烈……风格各异，陪伴长存。我们热爱音乐，热爱自由，热爱合作。音乐剧，solo/合唱比赛，校园文娱活动……社里的学长学姐以及亲爱的社友们都会给予大家最真诚的帮助！\n这里是key声乐社，期待你的加入！")
                        elif "汉风社" in msg:
                            sendMsg("机器人作者的主观评价：穿汉服的美女帅哥们")
                        elif ("Friday" in msg)or("friday" in msg)or("广播社" in msg):
                            sendMsg("机器人作者的主观评价：每周五都有广播，打扰我睡觉")
                        elif ("Stage" in msg)or("话剧社" in msg)or("stage" in msg):
                            sendMsg("机器人作者的主观评价：挺好看的")
                        elif ("penbeat" in msg)or("Penbeat" in msg):
                            sendMsg("机器人作者的主观评价：不常见")
                        elif "无线电" in msg:
                            sendMsg("机器人作者的主观评价：比较烧脑，还很累")
                        elif ("Encore" in msg)or("街舞社" in msg):
                            sendMsg("机器人作者的主观评价：很帅")
                        elif "IPA" in msg:
                            sendMsg("IPA(Information&Photography Association)信息技术与摄影协会，从2006年创办至今，已成为一中主力社团之一，其涉及面为：摄影、后期、微电影、网络信息技术。在聚光灯下度过这 15 年的IPA，也在这 15 年中聚焦了一中校园的点点滴滴，并积淀着15年以来历届社员的厚重情怀，从而散发出独一无二的魅力。")
                        elif ("二泉" in msg)or("文学社" in msg):
                            sendMsg("机器人作者的主观评价：可以提高语文成绩")
                        elif (("new" in msg)or("New" in msg))and(("age" in msg)or("Age" in msg))or("NA" in msg)or("管弦乐社" in msg)or("na" in msg)or("钠社" in msg):
                            sendMsg("New Age管弦乐社创办于2008年，社团多次荣获无锡市十佳社团，无锡市第一中学十佳社团等奖项。New Age是一中的老牌社团，在学生群体中有着不小的影响力，每次举办音乐会都会引起学校从老师到学生的广泛关注，乐团以高质量高水准而闻名。这里聚集了一群热爱古典，热爱音乐的学生，无论是古典交响乐，还是现代流行乐都不在话下，大家发挥自己的特长，传达着对于音乐的理解，给学校增添别样的的色彩。")
                        elif "密码社" in msg:
                            sendMsg("机器人作者的主观评价：猜密码的，但是和我撬锁又有什么关系呢")
                        elif ("武道社" in msg)or("乾元" in msg):
                            sendMsg("机器人作者的主观评价：武术操很帅")
                        elif ("Talkshow" in msg)or("talkshow" in msg):
                            sendMsg("机器人作者的主观评价：真的能讲")
                        elif ("相声社" in msg)or("龙鹤" in msg):
                            sendMsg("机器人作者的主观评价：太会了，节目特别精彩")
                        elif ("志愿者" in msg)or("志协" in msg):
                            sendMsg("机器人作者的主观评价：干杂活的，活动很多")
                        elif ("心理协会" in msg)or("心协" in msg):
                            sendMsg("机器人作者的主观评价：我很好奇好像有个叫恋爱咨询部2？")
                        elif "园艺社" in msg:
                            sendMsg("机器人作者的主观评价：种花的")
                        elif "羽毛球社" in msg:
                            sendMsg("机器人作者的主观评价：学校打羽毛球的很多")
                        elif ("伽绘" in msg)or("伽会" in msg)or("绘画社" in msg):
                            sendMsg("机器人作者的主观评价：和iMakeit合作过，画画的，上上届社长很喜欢mc")
                        elif "书法社" in msg:
                            sendMsg("机器人作者的主观评价：写字的，很少见")
                        #聊天相关功能

                        elif "彩蛋" in msg:
                            sendMsg('触发彩蛋致敬iMakeit')
                            sendMsg("我是群机器人，感谢来自iMakeit的技术支持\n快加入iMakeit！\niMakeit现任社长@新高二 iMakeit社长 徐闻远")
                        elif "一中小游戏" in msg:
                            sendMsg("乐趣尽在imakeit.tech")
                        elif ("美女" in msg) or ("帅哥" in msg) or ("美男子" in msg) or ("姐姐" in msg) or ("妹妹" in msg):
                            sendMsg("[CQ:face,id=2]")
                        elif "打你" in msg:
                            sendMsg("[CQ:face,id=5] 呜呜呜")
                        elif ("性别" in msg) or ("男" in msg) or ("女" in msg):
                            sendMsg("段景昊是男的，所以我是女的")
                        elif "你好" in msg:
                            sendMsg("你好我也好")
                        elif "欢迎鲍妈咪" in msg:
                            sendMsg("热烈欢迎鲍妈咪！")
                        elif "欢迎鲍老板" in msg:
                            sendMsg("热烈欢迎鲍老板！")
                        elif "bug" in msg:
                            sendMsg("bug bgu gbu gub ugb ubg bug remaining.....")
                        
                        #默认动作
                        else:
                            sendMsg("[CQ:poke,qq={}]".format(rev["user_id"]))       
            #清空缓存

            temp = ""

    finally:
        continue


